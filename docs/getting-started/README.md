# Getting Started

En esta sección se detallan conocimientos mínimos y herramientas que solemon usar en el equipo. Para referencia para posibles futuros integrantes del equipo.

## Conocimientos requeridos

En la siguiente página se detallan las tecnologías y librerías de las que se hace uso en **Platform**, principalmente para la parte de frontend. Además de contar con enlaces de interés hacia páginas de documentación o de cheat sheet para un fácil y rápido acceso directo a ellas.

[Conocimientos requeridos](./minimum-required-knowledge.md)

## Software y herramientas

En la siguiente página se detallan el software, herramientas y utilidades que se suelen usar a diario o frecuentemente.

[Software y herramientas](./required-tools.md)

## Enlaces de interés

> Es recomendado poner estas páginas en la barra de marcadores del navegador, para tenerlas siempre a mano

TODO