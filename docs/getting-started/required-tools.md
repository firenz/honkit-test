Aquí se detallan los software y herramientas necesarias para el equipo de Platform para el desarrollo web desde la perspectiva de FrontEnd.
# Software y herramientas 🔨

## Comunicación

### Slack ([link](https://slack.com/))

### Zoom ([link](https://zoom.us/))

## Desarrollo

### Google Chrome ([link](https://www.google.com/chrome/))

Necesario para poder testear y probar si los componentes de los proyectos funcionan correctamente.

> Es recomendable que inicies sesión en Google Chrome con tu cuenta de usuario de la empresa para que así se sincronicen de manera automática todos tus marcadores, extensiones, etc. por si en un futuro necesitas cambiar de PC o SO

#### Extensiones

- **Necesarias**
    - **Angular DevTools ([link](https://chrome.google.com/webstore/detail/angular-devtools/ienfalfjdbdpebioblfackkekamfmbnh))**
    - **Clear Cache ([link](https://chrome.google.com/webstore/detail/clear-cache/cppjkneekbjaeellbfkmgnhonkkjfpdn))**
    - **JWT Inspector ([link](https://chrome.google.com/webstore/detail/jwt-inspector/jgjihoodklabhdoeffdjofnknfijolgk?hl=en))**: para obtener el bearer token necesario para autorizar conexiones
    - **Allow CORS ([link](https://chrome.google.com/webstore/detail/allow-cors-access-control/lhobafahddgcelffkeicbaginigeejlf))**
- **Opcionales**
    - **UBlock Origin ([link](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm))**
    - **Onetab ([link](https://chrome.google.com/webstore/detail/onetab/chphlpgkkbolifaimnlloiipkdnihall))**: por si acumulas pestañas y poder cerrarlas y restaurarlas más adelante
    - **Bitwarden ([link](https://chrome.google.com/webstore/detail/bitwarden-free-password-m/nngceckbapebfimnlniiiahkandclblb))**: gestor de contraseñas open source y gratuito
- **Miscelánea**
    - **Grammarly ([link](https://chrome.google.com/webstore/detail/grammarly-for-chrome/kbfnbcaeplbcioakkpcpgfkobkghlhen))**: corrector de inglés para navegador
    - **BlockSite ([link](https://chrome.google.com/webstore/detail/blocksite-stay-focused-co/eiimnmioipafcokbfikbljfdeojpcgbh))**: para evitar distraerse demasiado navegando páginas

### Git ([link](https://git-scm.com/))

Bajar la versión correspondiente a tu SO. 

> También hay un [listado de aplicaciones Git con interfaz (Git GUI)](https://git-scm.com/downloads/guis/)  por si alguien se siente más cómodo que usando la terminal o el sistema de git integrado en VS Code. Yo personalmente recomiendo [Gitkraken](https://www.gitkraken.com/), aunque para repositorios privados es de pago.

### Node ([link](https://nodejs.org/en/))

Bajar siempre la última versión LTS.

> De manera opcional a Node, se puede instalar Node Version Manager (NVM) por si fuera necesario usar distinta versiones de Node entre proyectos.
> 
> * [Versión para Linux y macOS](https://github.com/nvm-sh/nvm)
> * [Versión para Windows](https://github.com/coreybutler/nvm-windows)

#### Paquetes npm globales a instalar

Una vez instalado node en nuestro SO, instalar los siguientes paquetes desde una terminal

```bash
npm install -g yarn
npm install -g @angular/cli
```

### VS Code ([link](https://code.visualstudio.com/))

IDE de desarrollo que usa la mayoría (si no todos) del equipo.

Recomendable activar la sincronización para que las extensiones y preferencias de VS Code se guarden y así si cambiamos de PC o de SO se mantengan.

- **Extensiones recomendadas**
    - **Visual Studio Intellisense ([link](https://marketplace.visualstudio.com/items?itemName=VisualStudioExptTeam.vscodeintellicode))**
    - **EditorConfig for VS Code ([link](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig))**
    - **Debugger for Chrome ([link](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome))**
    - **TSLint ([link](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin))**
    - **Prettier ([link](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode))**
    - **npm ([link](https://marketplace.visualstudio.com/items?itemName=eg2.vscode-npm-script))**
    - **npm Intellisense ([link](https://marketplace.visualstudio.com/items?itemName=christian-kohler.npm-intellisense))**
    - **Angular Language Service ([link](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template))**
    - **GrahQL ([link](https://marketplace.visualstudio.com/items?itemName=GraphQL.vscode-graphql))**
    - **Docker ([link](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker))**
    - **Markdown All in One ([link](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one))**
    - **Bookmarks ([link](https://marketplace.visualstudio.com/items?itemName=alefragnani.Bookmarks))**
    - **GitLens ([link](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens))**
    - **Auto Close Tag ([link](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag))**
    - **Auto Import ([link](https://marketplace.visualstudio.com/items?itemName=steoates.autoimport))**
    - **Auto Rename Tag ([link](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag))**
    - **Better Comments ([link](https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments))**
    - **Bracket Pair Colorized 2 ([link](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2))**
    - **Code Spell Checker ([link](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker))**
    - **Spanish - Code Spell Checker ([link](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-spanish))**
    - **Color Highlight ([link](https://marketplace.visualstudio.com/items?itemName=naumovs.color-highlight))**
    - **Highlight Matching Tag ([link](https://marketplace.visualstudio.com/items?itemName=vincaslt.highlight-matching-tag))**
    - **Import Cost ([link](https://marketplace.visualstudio.com/items?itemName=wix.vscode-import-cost))**
    - **indent-rainbow ([link](https://marketplace.visualstudio.com/items?itemName=oderwat.indent-rainbow))**
    - **Paste JSON as Code ([link](https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype))**
    - **Path Intellisense ([link](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense))**

### Postman ([link](https://www.postman.com/))

Para probar las conexiones de las APIs en local.

## Miscelánea

### Notepad++ ([link](https://notepad-plus-plus.org/))

### Docker ([link](https://www.docker.com/))

### LibreOffice ([link](https://www.libreoffice.org/))

### Photopea ([link](https://www.photopea.com/))