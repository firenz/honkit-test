# Conocimientos requeridos 🤔

> No es necesario ser un experto en todos estos conocimientos, pero si tener una base suficientemente sólida para poder moverte entre los proyectos con relativa soltura

## Typescript

Los proyectos se escriben en [Typescript](https://www.typescriptlang.org/), un superset de Javascript en el que se incluye tipados de variables y muchas otras funcionalidades.  

Si sólo has trabajado con Javascript ES06, es muy rápido a partir de ahí el aprender lo básico de Typescript para ir empezando.

### Mínimos a saber

- arrays/objects
- destructuring
- types/interfaces
- maps e iteradores
- try/catch
- async/await
- promesas
- JSON parsing

### Enlaces de interés

- [Documentación oficial de Typescript](https://www.typescriptlang.org/docs/)
- Official [Typescript Handbook](https://www.typescriptlang.org/docs/handbook/intro.html)
- [The Javascript's Beginner Handbook](https://flaviocopes.com/page/javascript-handbook/) por [Flavio Copes](https://flaviocopes.com/)

## Angular

Angular es el framework que usamos para desarrollar la parte frontend de las webs de los TGX.

### Mínimos a saber

- Componentes
- Decorators
- Services
- Routing
- Template Driven Forms & Reactive Forms (y validators)

### Pluses a saber

- Observables con RxJS: para crear/actualizar contenido reactivo por eventos en Angular
- Angular Universal: para crear páginas estáticas renderizadas desde servidor, para el SEO

### Enlaces de interés

- [Documentación oficial de Angular](https://angular.io/docs)
- [Documentación oficial de Angular Universal](https://angular.io/guide/universal)
- [Documentación oficial de RxJS](https://rxjs.dev/guide/overview)
- [Accessibility in Angular](https://angular.io/guide/accessibility)

## GraphQL

La mayoría de las llamadas a la API de TGX va a ser a través del gateway, que funciona con GraphQL. 

### Mínimos a saber

- Queries
- Mutations
- Variables

### Enlaces de interés

- [Documentación oficial de GraphQL](https://graphql.org/learn/)
- [A Beginner's Guide to GraphlQL](https://www.freecodecamp.org/news/a-beginners-guide-to-graphql-86f849ce1bec/) por [FreeCodeCamp](https://www.freecodecamp.org/)

## Git

Necesario para poder bajar los proyectos de TGX y poder subir los cambios que hagamos.

### Mínimos

- git add/rm
- git commit
- git checkout
- git pull/push
- git submodules

### Enlaces de interés

- [Documentación oficial de Git](https://git-scm.com/doc)
- [Git Cheat Sheet](https://www.freecodecamp.org/news/git-cheat-sheet/) por [FreeCodeCamp](https://www.freecodecamp.org/)
- [How to Use Branches in Git](https://www.freecodecamp.org/news/how-to-use-branches-in-git/) por [FreeCodeCamp](https://www.freecodecamp.org/)
- [GitFlow Tutorial](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow#:~:text=The%20overall%20flow%20of%20Gitflow,branch%20is%20created%20from%20develop&text=When%20a%20feature%20is%20complete%20it%20is%20merged%20into%20the,merged%20into%20develop%20and%20main) por Atlassian
- [Pull Request Workflow with Git](https://medium.com/@urna.hybesis/pull-request-workflow-with-git-6-steps-guide-3858e30b5fa4) por Hybesis en Medium

## Node, npm y yarn

Necesario para poder instalar las dependencias de los proyectos y hacerlos correr en local.

### Mínimos

Saber como funcionan los `package.json` necesarios para poder instalar los proyectos y como correr los comandos en la terminal tales como `npm install`, `npm start` o `yarn install` .

### Enlaces de interés

- [npm cheatsheet](https://devhints.io/npm) por [devhints.io](https://devhints.io/)
- [NPM Cheat Sheet](https://dev.to/duxtech/npm-cheat-sheet-4ak1) en Español por dux en Dev.to
- [yarn cheatsheet](https://devhints.io/yarn) por [devhints.io](https://devhints.io/)

## Opcionales pero recomendados

### ng-bootstrap

Para saber cómo funciona el estilado CSS de los proyectos. Este es el que usa en nuestros proyectos ya que se apoya en que usan Angular.

#### Enlaces de interés

- [ng-bootstrap official webpage](https://ng-bootstrap.github.io/#/home)

### Markdown

Para escribir la documentación de cómo arrancar los proyectos en los `[README.md](http://readme.md)` .

#### Enlaces de interés

- [Markdown Guide](https://www.markdownguide.org/)
- [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) por adam-p

### Jest y Cypress

En un futuro, queremos implementar testing en los proyectos de frontend. Jest funciona bien para tests de unidad e integración, mientras que Cypress funciona para tests de end2end (e2e) que permite simular comportamientos humanos de un usuario final.

#### Enlaces de interés

- [Documentación oficial de Jest](https://jestjs.io/docs/getting-started)
- [Documentación oficial de Cypress](https://docs.cypress.io/)

### HTML y CSS

Aunque de esta parte se encarga el maquetador del equipo, nunca está de más saber un poco por si se da la necesidad.

#### Enlaces de interés

- [HTML reference](https://developer.mozilla.org/en-US/docs/Web/HTML/Reference) por Mozilla
- [CSS reference](https://developer.mozilla.org/en-US/docs/Web/CSS) por Mozilla
- [CSS and JavaScript accessibility best practices](https://developer.mozilla.org/en-US/docs/Learn/Accessibility/CSS_and_JavaScript) por Mozilla