# Introducción

Esta wiki ha sido creada con el objetivo de facilitar el onboarding a nuevos miembros, además de ser una fuente de conocimiento a la que desarrolladores del equipo puedan recurrir si no se acuerdan de algo en concreto.