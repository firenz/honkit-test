# Summary

* [Introducción](./README.md)
* [Getting Started](./getting-started/README.md)
    * [Conocimientos mínimos](./getting-started/minimum-required-knowledge.md)
    * [Software y Herramientas](./getting-started/required-tools.md)

