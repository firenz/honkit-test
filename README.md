# honkit wiki test 
[![pipeline status](https://gitlab.com/firenz/honkit-test/badges/main/pipeline.svg)](https://gitlab.com/firenz/honkit-test/-/commits/main) 

Este es un repositorio de ejemplo hecho con [Honkit](https://github.com/honkit/honkit) para generar documentación en Markdown para un equipo de trabajo.

**[Enlace a la documentación generada](https://firenz.gitlab.io/honkit-test)**

## Cómo usar
En la carpeta raíz del proyecto
```bash
npm install # instalar dependencias
npm start # para probar en local los cambios
npm run build # para generar una build de la documentación en local
```

> Si lanzas `npm start` normalmente te cogerá todos los cambios en documentos ya existentes menos en `SUMMARY.md`. En ese caso hay que matar el proceso y volver a lanzarlo.

## Deploy a Gitlab Pages
Ya existe un fichero `.gitlab-ci.yml` configurado de manera que cualquier commit o PR mergeada a la rama `main` recrea una nueva build de la documentación.

## Referencias
[Documentación de Honkit](https://honkit.netlify.app/)
